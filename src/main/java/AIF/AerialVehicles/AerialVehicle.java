package AIF.AerialVehicles;


import AIF.AerialVehicles.Exceptions.*;
import AIF.Entities.Coordinates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AerialVehicle {
    protected int distanceTillMaintenance;
    protected boolean isOnGround;
    protected Coordinates currentLocation;
    private final int DEFAULT_FINAL_LIMIT = 10000;
    protected final Gear[] stations;

    public AerialVehicle(int distanceTillMaintenance, Coordinates currentLocation, Gear[] stations, String[] suitable, int stationNum) throws StationsInvalid {
        this.distanceTillMaintenance = distanceTillMaintenance;
        this.isOnGround = true;
        this.currentLocation = currentLocation;

        if (checkStationValidity(stations, suitable, stationNum)) {
            this.stations = stations;
        } else {
            throw new StationsInvalid();
        }
    }

    public boolean isOnGround() {
        return this.isOnGround;
    }

    public Coordinates currentLocation() {
        return this.currentLocation;
    }

    protected int distanceTillMaintenance() {
        return this.distanceTillMaintenance;
    }

    public void changeIsOnGround() {
        this.isOnGround = !this.isOnGround;
    }

    public void move(double distance) {
        this.distanceTillMaintenance -= distance;
    }

    public void takeOff() throws CannotPerformInMidAirException, NeedMaintenanceException {
        if (needMaintenance()) {
            throw new NeedMaintenanceException();
        } else if (!this.isOnGround) {
            throw new CannotPerformInMidAirException();
        } else {
            System.out.println("Taking off");
            changeIsOnGround();

        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformOnGroundException, NeedMaintenanceException {
        if (this.distanceTillMaintenance <= currentLocation().distance(destination)) {
            throw new NeedMaintenanceException();
        } else if (this.isOnGround) {
            throw new CannotPerformOnGroundException();
        } else {
            System.out.println("Flying to: " + destination);
            this.currentLocation = destination;
            move(currentLocation().distance(destination));
        }
    }

    public void land() throws CannotPerformOnGroundException {
        if (this.isOnGround) {
            throw new CannotPerformOnGroundException();
        } else {
            System.out.println("Landing");
            changeIsOnGround();
        }
    }

    public boolean needMaintenance() {
        return this.distanceTillMaintenance <= 0;
    }

    protected void performMaintenance(int distanceTillMaintenance) throws CannotPerformInMidAirException {
        if (!isOnGround) {
            throw new CannotPerformInMidAirException();
        } else {
            System.out.println("Performing maintenance");
            this.distanceTillMaintenance = distanceTillMaintenance;
        }
    }

    public void performMaintenance() throws CannotPerformInMidAirException {
        performMaintenance(DEFAULT_FINAL_LIMIT);
    }

    private static boolean checkStationValidity(Gear[] stations, String[] suitable, int stationsNum) {
        boolean validity = true;
        boolean singleValidity = false;

        if (stations.length == stationsNum) {
            for (Gear gear : stations) {

                for (String gearType : suitable) {
                    if (gear == null || gear.getClass().getName().equals(gearType)) {
                        singleValidity = true;
                        break;
                    }
                }

                if (!singleValidity) {
                    validity = false;
                    break;
                } else {
                    singleValidity = false;
                }
            }
        } else {
            validity = false;
        }
        return validity;
    }

    public void loadModule(Gear gearToLoad) throws NoModuleStationAvailableException {

        for (int i = 0; i < stations.length; i++) {
            if (stations[i] == null) {
                stations[i] = gearToLoad;
                System.out.println("gear added");
                return;
            }
        }

        throw new NoModuleStationAvailableException();
    }

    public void activateModule(String gearType, Coordinates location) throws ModuleNotFoundException {
        boolean isActivated = false;

        for (int i = 0; i < this.stations.length; i++) {
            if (this.stations[i] != null && this.stations[i].getClass().getName().equals(gearType)) {
                this.stations[i].activate();
                isActivated = true;
                System.out.println("module activated at " + location.toString());
            }
        }
        if (!isActivated) {
            throw new ModuleNotFoundException();
        }
    }

}
