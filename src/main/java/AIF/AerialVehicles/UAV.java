package AIF.AerialVehicles;

import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.AerialVehicles.Exceptions.NeedMaintenanceException;
import AIF.AerialVehicles.Exceptions.StationsInvalid;
import AIF.Entities.Coordinates;

public abstract class UAV extends AerialVehicle{

    public UAV(int distanceTillMaintenance, Coordinates currentLocation, Gear[] stations, String[] suitable, int stationNum) throws StationsInvalid {
        super(distanceTillMaintenance, currentLocation, stations, suitable, stationNum);
    }

    public void hoverOverLocation(double hours) throws CannotPerformOnGroundException, NeedMaintenanceException {
        if (this.distanceTillMaintenance <= hours*150) {
            throw new NeedMaintenanceException();
        } else if (this.isOnGround) {
            throw new CannotPerformOnGroundException();
        } else {
            System.out.println("Hovering over current location");
            this.move(hours*150);
        }
    }
}
