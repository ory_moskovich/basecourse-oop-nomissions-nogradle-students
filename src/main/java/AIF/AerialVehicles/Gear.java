package AIF.AerialVehicles;

public abstract class Gear {
    private boolean isActivated;

    public Gear() {
        this.isActivated = false;
    }

    public void activate() {
        this.isActivated = true;
    }


}
