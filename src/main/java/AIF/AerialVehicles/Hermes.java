package AIF.AerialVehicles;

import AIF.AerialVehicles.Exceptions.CannotPerformInMidAirException;
import AIF.AerialVehicles.Exceptions.StationsInvalid;
import AIF.Entities.Coordinates;

public abstract class Hermes extends UAV{
    private final static int DISTANCE_LIMIT = 25000;

    public Hermes(Coordinates currentLocation, Gear[] stations, String[] suitable, int stationNum) throws StationsInvalid {
        super(DISTANCE_LIMIT, currentLocation, stations, suitable, stationNum);
    }

    @Override
    public void performMaintenance() throws CannotPerformInMidAirException {
        super.performMaintenance(DISTANCE_LIMIT);
    }
}
