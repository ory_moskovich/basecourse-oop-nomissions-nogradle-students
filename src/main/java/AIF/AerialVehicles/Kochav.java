package AIF.AerialVehicles;

import AIF.AerialVehicles.Exceptions.StationsInvalid;
import AIF.Entities.Coordinates;

public class Kochav extends Hermes{

    private static final String[] SUITABLE = {Weapon.class.getName(), Sensor.class.getName(), PhotoPod.class.getName()};
    private static final int STATION_NUM = 5;

    public Kochav(Coordinates currentLocation, Gear[] stations) throws StationsInvalid {
        super(currentLocation, stations, SUITABLE, STATION_NUM);
    }

    public Kochav(Coordinates currentLocation) throws StationsInvalid {
        super(currentLocation, new Gear[STATION_NUM], SUITABLE, STATION_NUM);
    }
}
