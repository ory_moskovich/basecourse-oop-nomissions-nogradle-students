package AIF.AerialVehicles;


import AIF.AerialVehicles.Exceptions.StationsInvalid;
import AIF.Entities.Coordinates;

public class Zik extends Hermes{

    private static final String[] SUITABLE = {Weapon.class.getName(), PhotoPod.class.getName()};
    private static final int STATION_NUM = 1;

    public Zik(Coordinates currentLocation, Gear[] stations) throws StationsInvalid {
        super(currentLocation, stations, SUITABLE, STATION_NUM);
    }

    public Zik(Coordinates currentLocation) throws StationsInvalid {
        super(currentLocation, new Gear[STATION_NUM], SUITABLE, STATION_NUM);
    }
}