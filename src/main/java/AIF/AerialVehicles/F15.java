package AIF.AerialVehicles;


import AIF.AerialVehicles.Exceptions.StationsInvalid;
import AIF.Entities.Coordinates;

public class F15 extends FightingJet {
    private static final String[] SUITABLE = {Weapon.class.getName(), Sensor.class.getName()};
    private static final int STATION_NUM = 10;


    public F15(Coordinates currentLocation, Gear[] stations) throws StationsInvalid {
        super(currentLocation, stations, SUITABLE, STATION_NUM);
    }

    public F15(Coordinates currentLocation) throws StationsInvalid {
        super(currentLocation, new Gear[STATION_NUM], SUITABLE, STATION_NUM);
    }
}
