package AIF.AerialVehicles.Exceptions;

public class StationsInvalid extends AVException {
    public StationsInvalid() {
        super("Station invalid");
    }
}
