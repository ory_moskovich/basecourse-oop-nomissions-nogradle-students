package AIF.AerialVehicles;

import AIF.AerialVehicles.Exceptions.CannotPerformInMidAirException;
import AIF.AerialVehicles.Exceptions.StationsInvalid;
import AIF.Entities.Coordinates;


public abstract class FightingJet extends AerialVehicle {
    private static final int DISTANCE_LIMIT = 25000;


    public FightingJet(Coordinates currentLocation, Gear[] stations, String[] suitable, int stationNum) throws StationsInvalid {
        super(DISTANCE_LIMIT, currentLocation, stations, suitable, stationNum);
    }

    @Override
    public void performMaintenance() throws CannotPerformInMidAirException {
        super.performMaintenance(DISTANCE_LIMIT);
    }
}
