package AIF.AerialVehicles;


import AIF.AerialVehicles.Exceptions.StationsInvalid;
import AIF.Entities.Coordinates;

public class Shoval extends Haron{
    private static final String[] SUITABLE = {Weapon.class.getName(), Sensor.class.getName(), PhotoPod.class.getName()};
    private static final int STATION_NUM = 3;

    public Shoval(Coordinates currentLocation, Gear[] stations) throws StationsInvalid {
        super(currentLocation, stations, SUITABLE, STATION_NUM);
    }

    public Shoval(Coordinates currentLocation) throws StationsInvalid {
        super(currentLocation, new Gear[STATION_NUM], SUITABLE, STATION_NUM);
    }
}

